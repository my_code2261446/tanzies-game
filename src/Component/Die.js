import React from "react";
import '../App.css';
import dot from "./dot";

function Die(props){
    const style = {
        backgroundColor: props.held ? "#59E391" : "white"
    }

    const dots = function(){
        let dot = [];
        for(let i = 0; i < props.value; i++){
            return dot.map(() => <dot value={props.value} dotNumber={i} />)
        }
    }

    return(
            <div className="dice" style={style}>
                {dots}
            </div>
    );
}

export default Die;