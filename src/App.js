import './App.css';
import React from 'react';
import Die from './Component/Die';
import {nanoid} from "nanoid"

function App() {
  const [dice, setDice] = React.useState(allnewDice);
  const [tenzies, setTenzies] = React.useState(false);

  const displayDice = dice.map((die) => 
      <Die key={die.id} value={die.value} held={die.isHeld} hold={holdDice} id={die.id}/>
  )

  React.useEffect(() => {
    if(dice.every(die => die.value === dice[0].value) && dice.every(die => die.isHeld === true)){
      setTenzies(true);
      console.log("You won");
    }
  }, [dice])

  function rollNewDice(){
    if(tenzies){
      setTenzies(false);
      setDice(allnewDice());
    }else {
      setDice(oldDice => oldDice.map(die => {
        return die.isHeld ? die : {...die, value: randomNumber()}
      }));
    }
  }

  function randomNumber(){
    return Math.floor(Math.random() * 6);
  }

  function allnewDice(){
    const newDice = [];
    
    for(let i = 0; i < 10; i++){
      newDice.push({ 
        value: randomNumber(), 
        isHeld: false,
        id: nanoid()
      });
    }

    return newDice;
  }

  function holdDice(id){
    setDice(oldDice => oldDice.map(die => {
     return die.id === id ? {...die, isHeld: !die.isHeld} : die;
  }))
  }

  return (
    <main>
      <div className="tanzies">
          <h1 className="title">Tenzies</h1>
          <p className="instructions">Roll until all dice are the same. Click each die to freeze it at its current value between rolls.</p>
          <div className='dice-container'>
            {displayDice}
          </div>
          <button className='roll-btn' onClick={rollNewDice}>{tenzies ? "New Game" : "Roll"}</button>
      </div>
    </main>
  );
}

export default App;